rm -Rf /opt/node-10.15.3 2>/dev/null
rm -Rf /opt/ruby-2.6.1 2>/dev/null
rm -Rf /root/src/ror 2>/dev/null

NODE=$(ls node-v10.15.3-linux-x64.tar.xz | wc -l)
if [ "x$NODE" = "x0" ]; then
        echo
        echo "======================================================="
        echo "FALTA EL ARCHIVO node-v10.15.3-linux-x64.tar.xz"
        echo "======================================================="
        echo
        exit
fi
RUBY=$(ls ruby-2.6.1-gems.tar.gz | wc -l)
if [ "x$RUBY" = "x0" ]; then
        echo
        echo "======================================================="
        echo "FALTA EL ARCHIVO ruby-2.6.1-gems.tar.gz"
        echo "======================================================="
        echo
        exit
fi

xz -d node-v10.15.3-linux-x64.tar.xz
tar -xvf node-v10.15.3-linux-x64.tar
mv node-v10.15.3-linux-x64 /opt/node-10.15.3
tar -xvzf ruby-2.6.1-gems.tar.gz
mv opt/ruby-2.6.1 /opt/
rm -Rf opt

cat > /root/.bash_profile <<EOF
# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=/opt/python37/bin:/opt/go/bin:/opt/node-10.15.3/bin:/opt/ruby-2.6.1/bin:$PATH:$HOME/bin
LD_LIBRARY_PATH=/opt/openssl-1.1/lib:/usr/local/lib:/usr/lib64
export LD_LIBRARY_PATH

export PATH
EOF
sleep 1
source /root/.bash_profile

cat > /etc/sysconfig/iptables <<EOF
# Firewall configuration written by system-config-firewall
# Manual customization of this file is not recommended.
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 3306 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 8000 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 8080 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 8888 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 3000 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
-A FORWARD -j REJECT --reject-with icmp-host-prohibited
COMMIT
EOF
service iptables restart

mkdir -p /root/src/ror 2>/dev/null
tar -xvzf blog-rails.tar.gz
mv blog src/ror/

IP=$(ip addr | grep eth | grep inet | awk '{print $2}' | sed 's/\/24//g')
echo 
echo "============================================================================================="
echo "RUBY 2.6 y NODEJS 10.15 INSTALADO "
echo
echo "RUBY ON RAILS 5.2 INSTALADO"
echo
echo "IMPORTANTE: Cierre la sesion y vuelva a ingresar a Linux si hay problemas con Ruby o NodeJS"
echo "============================================================================================="

