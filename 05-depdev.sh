LIBGFORTRAN=$(ls libgfortran5-8.2.1-1.3.1.el6_10.x86_64.rpm | wc -l)
if [ "x$LIBGFORTRAN" = "x0" ]; then
        echo
        echo "========================================================================================================="
        echo "EL ARCHIVO libgfortran5-8.2.1-1.3.1.el6_10.x86_64.rpm NO SE HA ENCONTRADO, DEBE INCLUIRLO EN ESTA CARPETA"
        echo "========================================================================================================="
        echo
        exit
fi

CD=$(blkid /dev/sr0 | grep RHEL | wc -l)
if [ "x$CD" = "x0" ]; then
        echo "========================================="
        echo  POR FAVOR, INSERTE EL ISO/CD DE REDHAT
        echo "========================================="
        exit
fi
mount -t iso9660 /dev/cdrom /media
yum clean all

echo "Instalando dependencias..."
yum install libquadmath.x86_64 boost-date-time.x86_64 policycoreutils-python.x86_64 boost-thread.x86_64 boost-system.x86_64 devtoolset-8.x86_64 -y
sleep 1
rpm -i libgfortran5-8.2.1-1.3.1.el6_10.x86_64.rpm

echo
echo "==========================================="
echo "DEPENDENCIAS DE DESARROLLO INSTALADAS"
echo "==========================================="
umount -f /media
