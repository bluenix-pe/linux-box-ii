FILE1=$(ls phpMyAdmin-4.0.10.20-all-languages.tar.gz | wc -l)
FILE2=$(ls php-mbstring-5.3.3-49.el6.x86_64.rpm | wc -l)
if [ "x$FILE1" = "x0" ]; then
	echo "==========================================================="
	echo "FALTA EL ARCHIVO phpMyAdmin-4.0.10.20-all-languages.tar.gz"
	echo "==========================================================="
	exit
fi
if [ "x$FILE2" = "x0" ]; then
	echo "==========================================================="
	echo "FALTA EL ARCHIVO php-mbstring-5.3.3-49.el6.x86_64.rpm"
	echo "==========================================================="
	exit
fi

rm -rf /var/www/html/phpmyadmin 2>/dev/null
rm -rf phpMyAdmin-4.0.10.20-all-languages 2>/dev/null
rpm -i php-mbstring-5.3.3-49.el6.x86_64.rpm
tar -xvzf phpMyAdmin-4.0.10.20-all-languages.tar.gz
mv phpMyAdmin-4.0.10.20-all-languages/ /var/www/html/phpmyadmin
chown -R apache:apache /var/www/html/phpmyadmin
SITE=$(ip addr | grep eth | grep inet | awk '{print "http://" $2 "/phpmyadmin"}' | sed 's/\/24//g')
service iptables stop
service httpd stop
service httpd start
echo
echo
echo "==============================================="
echo "PhpMyAdmin ha sido subido al servidor web"
echo "Ingrese a: $SITE"
echo "==============================================="
