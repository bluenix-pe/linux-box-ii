touch /etc/yum.repos.d/rhscl.repo
cat > /etc/yum.repos.d/rhscl.repo <<EOF
[rhscl]
name=Red Hat Enterprise Linux Software Collection
baseurl=file:///media
enabled=1
gpgcheck=0
EOF

CD=$(blkid /dev/sr0 | grep RHSCL-3.2 | wc -l)
if [ "x$CD" = "x0" ]; then
        echo "======================================================================"
        echo  POR FAVOR, INSERTE EL ISO **"rhscl-server-3.2-rhel-6-x86_64-dvd.iso"**
        echo "======================================================================"
        exit
fi
mount -t iso9660 /dev/cdrom /media
yum clean all
yum install devtoolset-8.x86_64 -y
