rm -Rf /opt/openssl-1.1 2>/dev/null
rm -Rf /opt/python37 2>/dev/null
rm -Rf /opt/go 2>/dev/null

LIBFFI=$(ls libffi-devel-3.0.5-3.2.el6.x86_64.rpm | wc -l)
if [ "x$LIBFFI" = "x0" ]; then
        echo
        echo "======================================================="
        echo "FALTA EL ARCHIVO libffi-devel-3.0.5-3.2.el6.x86_64.rpm"
        echo "======================================================="
        echo
        exit
fi
PYTHON37=$(ls python37-pip.tar.gz | wc -l)
if [ "x$PYTHON37" = "x0" ]; then
        echo
        echo "======================================================="
        echo "FALTA EL ARCHIVO python37-pip.tar.gz"
        echo "======================================================="
        echo
        exit
fi
OPENSSL=$(ls openssl-1.1-bin.tar.gz | wc -l)
if [ "x$OPENSSL" = "x0" ]; then
        echo
        echo "======================================================="
        echo "FALTA EL ARCHIVO openssl-1.1-bin.tar.gz"
        echo "======================================================="
        echo
        exit
fi

GOLANG=$(ls go1.12.linux-amd64.tar.gz | wc -l)
if [ "x$GOLANG" = "x0" ]; then
        echo
        echo "======================================================="
        echo "FALTA EL ARCHIVO go1.12.linux-amd64.tar.gz"
        echo "======================================================="
        echo
        exit
fi


tar -xvzf openssl-1.1-bin.tar.gz
tar -xvzf python37-pip.tar.gz
tar -xvzf go1.12.linux-amd64.tar.gz
mv opt/openssl-1.1 /opt
mv opt/python37 /opt
mv go /opt
rm -Rf opt

cat > /root/.bash_profile <<EOF
# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=/opt/python37/bin:/opt/go/bin:$PATH:$HOME/bin
LD_LIBRARY_PATH=/opt/openssl-1.1/lib:/usr/local/lib:/usr/lib64
export LD_LIBRARY_PATH

export PATH
EOF
sleep 1
source /root/.bash_profile

cat > /etc/sysconfig/iptables <<EOF
# Firewall configuration written by system-config-firewall
# Manual customization of this file is not recommended.
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 3306 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 8000 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 8080 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 8888 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
-A FORWARD -j REJECT --reject-with icmp-host-prohibited
COMMIT
EOF
service iptables restart

mkdir /root/labs 2>/dev/null

IP=$(ip addr | grep eth | grep inet | awk '{print $2}' | sed 's/\/24//g')
echo 
echo "============================================================================================="
echo "PYTHON 3.7 y GO 1.12 INSTALADO "
echo "Consola  : python3"
echo "Notebook : jupyter notebook --no-browser --allow-root --ip=$IP --notebook-dir=/root/labs"
echo
echo "IMPORTANTE: Cierre la sesion y vuelva a ingresar a Linux si hay problemas con Python o Go"
echo "============================================================================================="

