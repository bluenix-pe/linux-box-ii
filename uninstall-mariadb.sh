REPO_RHSCL=$(ls /etc/yum.repos.d/rhscl.repo | wc -l)
if [ "x$REPO_RHSCL" = "x0" ]; then
        echo "============================================================"
        echo "FALTA REPOSITORIO RHSCL, EJECUTE PRIMERO: sh 05-depdev.sh"
        echo "============================================================"
        exit
fi


CD=$(blkid /dev/sr0 | grep RHSCL-3.2 | wc -l)
if [ "x$CD" = "x0" ]; then
        echo "======================================================================"
        echo  POR FAVOR, INSERTE EL ISO **rhscl-server-3.2-rhel-6-x86_64-dvd.iso**
        echo "======================================================================"
        exit
fi
mount -t iso9660 /dev/cdrom /media
yum clean all
service mysqld stop 2>/dev/null
service rh-mariadb102-mariadb stop
chkconfig rh-mariadb102-mariadb off
sleep 1
yum remove rh-mariadb102.x86_64 -y
sleep 1
yum remove rh-mariadb102* -y
rm -Rf /var/opt/rh/rh-mariadb102/
rm -Rf /opt/rh/httpd24/root/var/www/html/phpmyadmin

service mysqld start
chkconfig mysqld on
service iptables restart
SITE=$(ip addr | grep eth | grep inet | awk '{print "http://" $2 "/phpmyadmin"}' | sed 's/\/24//g')
echo
echo "=============================================================================="
echo " MARIADB DESINSTALADO"
echo
echo " IMPORTANTE: Se ha restablecido MYSQL 5.1"
echo "=============================================================================="
