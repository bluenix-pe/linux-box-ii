rm -Rf /opt/node-10.15.3 2>/dev/null
rm -Rf /opt/ruby-2.6.1 2>/dev/null
rm -Rf /opt/python37 2>/dev/null
rm -Rf /opt/go 2>/dev/null
rm -Rf /opt/openssl-1.1 2>/dev/null

cat > /root/.bash_profile <<EOF
# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=/usr/local/sbin:/sbin:/bin:/usr/sbin:/usr/bin:$HOME/bin
export PATH
EOF
sleep 1
source /root/.bash_profile

cat > /etc/sysconfig/iptables <<EOF
# Firewall configuration written by system-config-firewall
# Manual customization of this file is not recommended.
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 3306 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
-A FORWARD -j REJECT --reject-with icmp-host-prohibited
COMMIT
EOF
service iptables restart


echo "=========================================="
echo "PYTHON 3.7 DESINSTALADO"
echo "GO 1.12 DESINSTALADO"
echo "NODE 10.15 DESINSTALADO"
echo "RUBY 2.6 DESINSTALADO"
echo
echo "Nota: Debe cerrar sesion y volver a abrir"
echo "=========================================="
