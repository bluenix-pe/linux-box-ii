REPO_RHSCL=$(ls /etc/yum.repos.d/rhscl.repo | wc -l)
if [ "x$REPO_RHSCL" = "x0" ]; then
        echo "============================================================"
        echo "FALTA REPOSITORIO RHSCL, EJECUTE PRIMERO: sh 05-depdev.sh"
        echo "============================================================"
        exit
fi


CD=$(blkid /dev/sr0 | grep RHSCL-3.2 | wc -l)
if [ "x$CD" = "x0" ]; then
        echo "======================================================================"
        echo  POR FAVOR, INSERTE EL ISO **rhscl-server-3.2-rhel-6-x86_64-dvd.iso**
        echo "======================================================================"
        exit
fi
mount -t iso9660 /dev/cdrom /media
yum clean all
service mysqld stop 2>/dev/null
chkconfig mysqld off
sleep 1
yum install rh-mariadb102.x86_64 -y
sleep 1

service mysqld stop 2>/dev/null
service rh-mariadb102-mariadb start
chkconfig rh-mariadb102-mariadb on

scl enable rh-mariadb102 -- '/opt/rh/rh-mariadb102/root/usr/bin/mysql_secure_installation' <<EOF

y
123456
123456
y
y
y
y
EOF

rm -Rf /opt/rh/httpd24/root/var/www/html/phpmyadmin 2>/dev/null
tar -xvzf phpMyAdmin-4.8.5-all-languages.tar.gz 1>/dev/null
mv phpMyAdmin-4.8.5-all-languages /opt/rh/httpd24/root/var/www/html/phpmyadmin
SITE=$(ip addr | grep eth | grep inet | awk '{print "http://" $2 "/phpmyadmin"}' | sed 's/\/24//g')

service iptables restart

echo '<?php phpinfo(); ?>' >/opt/rh/httpd24/root/var/www/html/info.php
echo
echo "=============================================================================================="
echo " MARIADB INSTALADO"
echo
echo " Consola: scl enable rh-mariadb102 -- '/opt/rh/rh-mariadb102/root/usr/bin/mysql' -u root -p"
echo " PhpMyADmin: $SITE"
echo "=============================================================================================="
