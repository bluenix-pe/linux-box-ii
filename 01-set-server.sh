touch /etc/yum.repos.d/dvd.repo
cat > /etc/yum.repos.d/dvd.repo <<EOF
[dvd]
name=Red Hat Enterprise Linux DVD
baseurl=file:///media
enabled=1
gpgcheck=0
EOF

CD=$(blkid /dev/sr0 | grep RHEL | wc -l)
if [ "x$CD" = "x0" ]; then
        echo "========================================="
        echo  POR FAVOR, INSERTE EL ISO/CD DE REDHAT
        echo "========================================="
        exit
fi
mount -t iso9660 /dev/cdrom /media
yum clean all

service network restart
chattr -i /etc/resolv.conf
cat > /etc/resolv.conf <<EOF
domain localdomain
nameserver 127.0.0.1
EOF
chattr +i /etc/resolv.conf

sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
setenforce 0

cat > /etc/sysconfig/iptables <<EOF
# Firewall configuration written by system-config-firewall
# Manual customization of this file is not recommended.
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT
-A INPUT -m state --state NEW -m tcp -p tcp --dport 3306 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
-A FORWARD -j REJECT --reject-with icmp-host-prohibited
COMMIT
EOF

chkconfig mysqld on
chkconfig httpd on
echo
echo
echo "========================================="
echo  SERVIDOR CONFIGURADO Y LISTO PARA USARSE
echo "========================================="
