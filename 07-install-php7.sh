REPO_RHSCL=$(ls /etc/yum.repos.d/rhscl.repo | wc -l)
if [ "x$REPO_RHSCL" = "x0" ]; then
        echo "============================================================"
        echo "FALTA REPOSITORIO RHSCL, EJECUTE PRIMERO: sh 05-depdev.sh"
        echo "============================================================"
        exit
fi


CD=$(blkid /dev/sr0 | grep RHSCL-3.2 | wc -l)
if [ "x$CD" = "x0" ]; then
        echo "======================================================================"
        echo  POR FAVOR, INSERTE EL ISO **rhscl-server-3.2-rhel-6-x86_64-dvd.iso**
        echo "======================================================================"
        exit
fi
mount -t iso9660 /dev/cdrom /media
yum clean all
service httpd stop 2>/dev/null
chkconfig httpd off
sleep 1
yum install httpd24 httpd24-mod_ssl httpd24-httpd-manual -y
sleep 1
yum install rh-php70 rh-php70-php rh-php70-php-mysqlnd rh-php70-php-mbstring -y

service httpd stop 2>/dev/null
service httpd24-httpd start
chkconfig httpd24-httpd on
#cp -Rf /var/www/html/phpmyadmin/ /opt/rh/httpd24/root/var/www/html/ 2>/dev/null
SITE=$(ip addr | grep eth | grep inet | awk '{print "http://" $2 "/info.php"}' | sed 's/\/24//g')
echo '<?php phpinfo(); ?>' >/opt/rh/httpd24/root/var/www/html/info.php
echo
echo "=============================================================================="
echo " PHP7 INSTALADO"
echo " Ingrese a: $SITE"
echo
echo " IMPORTANTE:"
echo " Las paginas deben ser colocadas en: /opt/rh/httpd24/root/var/www/html/"
echo "=============================================================================="
