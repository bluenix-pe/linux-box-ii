chattr -i /etc/resolv.conf
cat > /etc/resolv.conf << EOF
domain localdomain
nameserver 127.0.0.1
nameserver 8.8.8.8
EOF
chattr +i /etc/resolv.conf
