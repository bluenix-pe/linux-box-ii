INSTALLED=$(ls /usr/bin/vmware-uninstall-tools.pl | wc -l)
if [ "x$INSTALLED" = "x1" ]; then
        echo "==================================================="
        echo "LAS VMWARE TOOLS YA ESTAN INSTALADAS"
        echo "==================================================="
        exit
fi

CD=$(blkid /dev/sr0 | grep VMware | wc -l)
if [ "x$CD" = "x0" ]; then
        echo "==================================================="
	echo "POR FAVOR, SELECCIONE EL MENU 'Install VMWare Tools'"
        echo "==================================================="
        exit
fi
umount -f /media 2>/dev/null
sleep 1
mount -t iso9660 /dev/cdrom /media

VMTOOL=$(ls /media/VMwareTools* | wc -l)
if [ "x$VMTOOL" = "x0" ]; then
	echo
	echo "===================================================="
	echo "POR FAVOR, SELECCIONE EL MENU 'Install VMWare Tools'"
	echo "===================================================="
	echo
	exit
fi

cp VMwareTools* /tmp
cd /tmp
FILE=$(ls VMwareTools*)
tar -xvzf $FILE
cd vmware-tools-distrib
./vmware-install.pl -d default
sleep 1
cd
rm -Rf /tmp/vmware-tools-distrib
echo
echo
echo "=============================================================="
echo "POR FAVOR, ASEGURESE DE COLOCAR NUEVAMENTE EL ISO/CD DE REDHAT"
echo "=============================================================="
echo
read -p "Presiones cualquier tecla para continuar... "
echo
echo "El servidor se va a reiniciar en 5 segundos..."
sleep 5
init 6
