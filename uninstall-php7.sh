REPO_RHSCL=$(ls /etc/yum.repos.d/rhscl.repo | wc -l)
if [ "x$REPO_RHSCL" = "x0" ]; then
        echo "============================================================"
        echo "FALTA REPOSITORIO RHSCL, EJECUTE PRIMERO: sh 05-depdev.sh"
        echo "============================================================"
        exit
fi


CD=$(blkid /dev/sr0 | grep RHSCL-3.2 | wc -l)
if [ "x$CD" = "x0" ]; then
        echo "======================================================================"
        echo  POR FAVOR, INSERTE EL ISO **rhscl-server-3.2-rhel-6-x86_64-dvd.iso**
        echo "======================================================================"
        exit
fi
mount -t iso9660 /dev/cdrom /media
yum clean all
service httpd stop 2>/dev/null
service httpd24-httpd stop
chkconfig httpd24-httpd off
sleep 1
yum remove httpd24 httpd24-mod_ssl httpd24-httpd-manual -y
sleep 1
yum remove rh-php70 rh-php70-php rh-php70-php-mysqlnd rh-php70-php-mbstring -y
yum remove rh-php7* -y

service httpd start
chkconfig httpd on
echo '<?php phpinfo(); ?>' >/var/www/html/info.php
SITE=$(ip addr | grep eth | grep inet | awk '{print "http://" $2 "/phpmyadmin"}' | sed 's/\/24//g')
echo
echo "=============================================================================="
echo " PHP7 DESINSTALADO"
echo
echo " IMPORTANTE: Se ha restablecido APACHE 2.2 y PHP 5.3.3"
echo "=============================================================================="
